(function($, window, document, Drupal) {

	$(function() {
		console.log('flickr');
		$('.flickr-album-full__image a').colorbox({
			maxWidth: '90%',
			maxHeight: '90%',
		});
	});

	$(window).on('load', function() {
		$('.flickr-album-full__images').isotope({
			itemSelector: '.flickr-album-full__image',
		});
	});

})(jQuery, window, document, Drupal);