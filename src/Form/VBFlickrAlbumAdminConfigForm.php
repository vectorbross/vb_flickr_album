<?php

namespace Drupal\vb_flickr_album\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;


/**
 * Provides a form for configuring the Flickr Album node type.
 */
class VBFlickrAlbumAdminConfigForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vb_flickr_album_admin_config_form';
  }

  /**
   * Class constructor.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, LanguageManagerInterface $language_manager) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entity_type_manager;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'vb_flickr_album.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('vb_flickr_album.settings');
    $node_storage = $this->entityTypeManager->getStorage('node');
    $overview = $config->get('overview');

    // if (!$this->apiKey || !$this->apiSecret) {
    //   $msg = $this->t('Flickr API credentials are not set. It can be set on the <a href=":config_page">configuration page</a>.',
    //     [':config_page' => Url::fromRoute('flickr_api.settings')]
    //   );

    //   $this->messenger->addError($msg);
    //   return FALSE;
    // }

    if(empty(\Drupal::config('flickr_api.settings')->get('api_key')) || empty(\Drupal::config('flickr_api.settings')->get('api_secret'))) {
      $form['warning'] = [
        '#markup' => '<div class="messages messages--error">' . t('Flickr API credentials are not set. It can be set on the <a href=":config_page">configuration page</a>.', [':config_page' => Url::fromRoute('flickr_api.settings')->toString()]) . '</div>'
      ];
    }

    $form['overview'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Overview page'),
      '#target_type' => 'node',
      '#autocreate' => [
        'bundle' => ['page'],
      ],
      '#default_value' => $overview ? $node_storage->load($overview) : NULL,
    ];
    $form['detail'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show a <em>back to overview</em> button on the album detail page'),
      '#default_value' => $config->get('detail'),
      '#states' => [
        'visible' => [
          '#edit-overview' => ['filled' => TRUE],
        ],
      ]
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $node_storage = $this->entityTypeManager->getStorage('node');
    $overview = $node_storage->load($values['overview']);
    $this->configFactory->getEditable('vb_flickr_album.settings')
      ->set('overview', $values['overview'])
      ->set('detail', $values['detail'])
      ->save();

    // generate pathauto patterns
    foreach($this->languageManager->getLanguages() as $langcode => $language) {
      $this->configFactory->getEditable('pathauto.pattern.flickr_album')
        ->setData([
          'langcode' => $langcode,
          'status' => TRUE,
          'dependencies' => [
            'module' => [
              'node',
            ],
          ],
          'id' => 'flickr_album',
          'label' => $this->t('Flickr album'),
          'type' => 'canonical_entities:node',
          'pattern' => (!empty($overview) && $overview->hasTranslation($langcode) ? $overview->getTranslation($langcode)->toUrl()->toString() . '/[node:title]' : '[node:title]'),
          'selection_criteria' => [
            [
              'id' => 'node_type',
              'bundles' => [
                'flickr_album' => 'flickr_album',
              ],
              'negate' => FALSE,
              'context_mapping' => [
                'node' => 'node',
              ],
            ]
          ],
          'selection_logic' => 'and',
          'weight' => -5,
          'relationships' => [],
        ])
        ->save();
    }

    // regenerate path aliases
    $query = \Drupal::entityQuery('node');
    $query->condition('status', 1);
    $query->condition('type', 'flickr_album');
    $entity_ids = $query->accessCheck()->execute();

    foreach ($entity_ids AS $nid) {
      $node = Node::load($nid);
      $node->set("path", ["pathauto" => TRUE]);
      $node->save();
    }

    parent::submitForm($form, $form_state);
  }

}
